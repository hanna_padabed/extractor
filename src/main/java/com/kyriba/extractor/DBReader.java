package com.kyriba.extractor;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import static com.kyriba.extractor.DataExtractor.emptyIfNull;


public class DBReader
{
  private static final String DATABASE_CONFIG_FILE = "database.properties";

  private String dbHost;
  private String dbPort;
  private String dbService;
  private String dbUser;
  private String dbPassword;


  public DBReader(CommandArgs commandArgs) throws IOException
  {
    loadDBProperties(commandArgs);
  }


  public Map<String, String> read(EntityType entity) throws SQLException
  {
    Map<String, String> result = new LinkedHashMap<>();
    try (Connection connection = DriverManager.getConnection(getDBUrl(), dbUser, dbPassword)) {
      Statement stmt = connection.createStatement();
      ResultSet rs = stmt.executeQuery(entity.getQuery());
      while (rs.next()) {
        String code = rs.getString(1);
        String description = rs.getString(2);
        result.put(code, description);
      }
    }
    return result;
  }


  public JSONArray readWithAdditionalData(EntityType entity) throws SQLException
  {
    JSONArray result = new JSONArray();
    try (Connection connection = DriverManager.getConnection(getDBUrl(), dbUser, dbPassword)) {
      Statement stmt = connection.createStatement();
      ResultSet rs = stmt.executeQuery(entity.getAdditionalQuery());
      while (rs.next()) {
        JSONObject value = new JSONObject()
            .put("code", emptyIfNull(rs.getString(1)))
            .put("description", emptyIfNull(rs.getString(2)))
            .put(entity.getAdditionalParameterName(), new JSONObject()
                .put("code", emptyIfNull(rs.getString(3)))
                .put("description", emptyIfNull(rs.getString(4))));
        result.put(value);
      }
    }
    return result;
  }


  private void loadDBProperties(CommandArgs commandArgs) throws IOException
  {
    Properties configuration = new Properties();
    configuration.load(DataExtractor.class.getClassLoader().getResourceAsStream(DATABASE_CONFIG_FILE));
    dbHost = commandArgs.dbHost != null ? commandArgs.dbHost : configuration.getProperty("db_host");
    dbPort = commandArgs.dbPort != null ? commandArgs.dbPort : configuration.getProperty("db_port");
    dbService = commandArgs.dbService != null ? commandArgs.dbService : configuration.getProperty("db_service");
    dbUser = commandArgs.dbUser != null ? commandArgs.dbUser : configuration.getProperty("db_user");
    dbPassword = commandArgs.dbPassword != null ? commandArgs.dbPassword : configuration.getProperty("db_password");
  }


  private String getDBUrl()
  {
    return String.format("jdbc:oracle:thin:@%s:%s/%s", dbHost, dbPort, dbService);
  }


}