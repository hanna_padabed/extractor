package com.kyriba.extractor;

import com.beust.jcommander.Parameter;


public class CommandArgs
{
  @Parameter(names = { "--entity", "-e" }, description = "Entity to retrieve", required = true)
  public String entity;

  @Parameter(names = { "--host", "-h" }, description = "Database host. Default: localhost")
  public String dbHost;
  @Parameter(names = { "--port", "-p" }, description = "Database port. Default: 1521")
  public String dbPort;
  @Parameter(names = { "--service", "-s" }, description = "Database service. Default: xe")
  public String dbService;
  @Parameter(names = { "--user", "-u" }, description = "Database user. Default: KVDEV")
  public String dbUser;
  @Parameter(names = { "--password", "-ps" }, description = "Database password. Default: KYRIBA", password = true)
  public String dbPassword;

  @Parameter(names = { "--json", "-j" }, description = "Output format: json")
  public boolean isJson;
  @Parameter(names = { "--additional", "-a" },
      description = "Retrieve additional info, e.g. third party account together with third party. Only with JSON flag")
  public boolean isAdditional;
  @Parameter(names = { "--write", "-w" }, description = "Write result to file: output.json")
  public boolean isWriteToFile;
}
