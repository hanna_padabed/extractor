package com.kyriba.extractor;

import com.beust.jcommander.JCommander;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Map;
import java.util.stream.Collectors;


public class DataExtractor
{
  private static final String DELIMITER = ";";


  public static void main(String[] args)
  {
    CommandArgs commandArgs = new CommandArgs();
    JCommander commander = JCommander.newBuilder().addObject(commandArgs).build();
    if (args.length == 0) {
      commander.usage();
      return;
    }
    try {
      commander.parse(args);
      if (commandArgs.isAdditional && !commandArgs.isJson)
        throw new RuntimeException("Additional data can be retrieved only for json format");
      new DataExtractor().extract(commandArgs);
    }
    catch (Exception ex) {
      System.out.println(ex.getMessage());
    }
  }


  public void extract(CommandArgs commandArgs) throws Exception
  {
    String result;
    DBReader dbReader = new DBReader(commandArgs);
    EntityType entity = EntityType.valueOfIgnoreCase(commandArgs.entity);
    if (commandArgs.isJson && commandArgs.isAdditional) {
      result = dbReader.readWithAdditionalData(entity).toString();
    }
    else {
      Map<String, String> entities = dbReader.read(entity);
      if (commandArgs.isJson) {
        result = new JSONArray(entities.entrySet().stream()
            .map(it -> new JSONObject()
                .put("code", emptyIfNull(it.getKey()))
                .put("description", emptyIfNull(it.getValue())))
            .collect(Collectors.toList())).toString();
      }
      else
        result = String.join(DELIMITER, entities.keySet());
    }

    print(commandArgs, result);
  }


  private void print(CommandArgs commandArgs, String data) throws IOException
  {
    if (commandArgs.isWriteToFile) {
      FileOutputStream fos = new FileOutputStream("output" + (commandArgs.isJson ? ".json" : ".txt"));
      BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
      bw.write(data);
      bw.close();
    }
    else System.out.println(data);
  }


  static String emptyIfNull(String data)
  {
    return data == null ? "" : data;
  }

}
