Entity code and description extractor
======
 
## Build

```
./gradlew build
```

The result jar will be at `/build/libs/Extractor.jar`

## Command line options
Required:

`--entity, -e`
  Entity to retrieve


Flags:

`--json, -j`
  Output format: json
  Default: false
  
  
`--write, -w`
  Write result to file: output.json
  Default: false
  
  
`--additional, -a`
  Retrieve additional info, e.g. third party account together with third 
  party. Only with JSON flag
  Default: false
  
    
DB parameters (They also can be changed from `database.properties` file):


`--host, -h`
  Database host. Default: localhost
  
  
`--password, -ps`
  Database password. Default: KYRIBA
  
  
`--port, -p`
  Database port. Default: 1521
  
  
`--service, -s`
  Database service. Default: xe
  
  
`--user, -u`
  Database user. Default: KVDEV

## Run

View the `;`separated list of third parties' codes on the console:
```
 java -jar ./build/libs/Extractor.jar -e third_party
```

View the `;`separated list of third parties' codes in the file `output.txt`:
```
 java -jar ./build/libs/Extractor.jar -e third_party -w
```

View the list of third parties' codes and descriptions in the JSON format on the console:
```
 java -jar ./build/libs/Extractor.jar -e third_party -j
```

View the list of third parties' codes and descriptions in the JSON format in the file `output.json`:
```
 java -jar ./build/libs/Extractor.jar -e third_party -w -j
```

View the list of third parties' codes and descriptions with additional info (third party account info) in the JSON format in the file `output.json`:
```
 java -jar ./build/libs/Extractor.jar -e third_party -w -j -a
```

## Supported entities
  * company
  * company_group
  * bank
  * branch 
  * company_bank_account
  * currency
  * country
  * transaction_code
  * transaction_category
  * transaction_type
  * budget_code
  * interest_rate_type
  * link_number
  * portfolio
  * account_category
  * account_group
  * bank_transaction_code
  * actor
  * business_calendar
  * cash_flow_code
  * file_code
  * gl_account_type
  * invoice_type
  * link_number_code
  * ne_netting_center
  * ne_netting_cycle
  * ne_netting_participant
  * py_assignment_rule
  * signer_group
  * template_type
  * third_party_category
  * user_zone